//
//  main.swift
//  bluetooth
//
//  Created by Romain Petit on 13/02/2019.
//  Copyright © 2019 Romain Petit. All rights reserved.
//

import Foundation
import CoreBluetooth

let dispatchGroup = DispatchGroup()
let controller = Controller()
let server = HttpServer()

dispatchGroup.enter()
//bluetooth
DispatchQueue.global(qos:.userInteractive).async{
    controller.start()
}
//http server
DispatchQueue.global(qos:.userInteractive).async{
    
    server["/"] = { .ok(.html("You asked for \($0)"))  }

    server.POST["/in"] = { request in
        let req_body = String(bytes: request.body, encoding: .utf8)
        controller.send(command: req_body!);
        return .ok(.html("ok"))
    }
    
    let semaphore = DispatchSemaphore(value: 0)
    do {
        try server.start(9080, forceIPv4: true)
        print("Server has started ( port = \(try server.port()) ). Try to connect now...")
        semaphore.wait()
    } catch {
        print("Server start error: \(error)")
        semaphore.signal()
    }
}
//console
DispatchQueue.global(qos:.userInteractive).async{
    var response: Optional<String> = Optional.init("")
    while(response! != "quit" ){
        response = readLine()
        print(">"+response!)
        if(response?.starts(with: "send ") ?? false){
            let r = response!
            let indexStartOfText = r.index(r.startIndex, offsetBy: 5)
            controller.send(command: String(r[indexStartOfText...]));
        }
    }
    //todo stop controller
    exit(EXIT_SUCCESS)
}
//
dispatchGroup.notify(queue: DispatchQueue.main) {
    exit(EXIT_SUCCESS)
}
dispatchMain()

