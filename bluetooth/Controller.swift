//
//  Controller.swift
//  bluetooth
//
//  Created by Romain Petit on 13/02/2019.
//  Copyright © 2019 Romain Petit. All rights reserved.
//
import Foundation
import CoreBluetooth

class Controller: NSObject,CBCentralManagerDelegate, CBPeripheralDelegate {
    
    var central:CBCentralManager
    var started:Bool = false
    
    let MoveHub_Service_UUID = CBUUID(string: "00001623-1212-EFDE-1623-785FEABCD123")
    let MoveHub_Characteristic_UUID = CBUUID(string: "00001624-1212-EFDE-1623-785FEABCD123")
    
    var connectedPeripheral:CBPeripheral? = nil
    var selectedService:CBService? = nil
    var selectedCharacteristic:CBCharacteristic? = nil

    override init() {
        print("initialization...")
        central = CBCentralManager(delegate: nil, queue: nil)
        super.init()
        central.delegate = self
        print("initialized")
    }
    
    //this method is called based on
    // the device's Bluetooth state; we can ONLY
    // scan for peripherals if Bluetooth is .poweredOn
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("Did update central manager state")
        switch central.state {
            case .unknown:
                print("Bluetooth status is UNKNOWN")
            case .resetting:
                print("Bluetooth status is RESETTING")
            case .unsupported:
                print("Bluetooth status is UNSUPPORTED")
            case .unauthorized:
                print("Bluetooth status is UNAUTHORIZED")
            case .poweredOff:
                print("Bluetooth status is POWERED OFF")
            case .poweredOn:
                print("Bluetooth status is POWERED ON")
                startOpportunity();
        }
    }
    
    //discover what peripheral devices OF INTEREST
    // are available for this app to connect to
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral,
        advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print("Connecting...")
        connectedPeripheral = peripheral
        connectedPeripheral?.delegate = self
        central.stopScan()
        central.connect(connectedPeripheral!)
    }
    
    //Invoked when a connection is successfully created with a peripheral.
    // we can only move forwards when we know the connection
    // to the peripheral succeeded
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Connected!")
        // look for services of interest on peripheral
        connectedPeripheral?.discoverServices([MoveHub_Service_UUID])
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        for service in peripheral.services! {
            if service.uuid == MoveHub_Service_UUID {
                print("Service: \(service)")
                selectedService = service
                // STEP 9: look for characteristics of interest
                // within services of interest
                peripheral.discoverCharacteristics(nil, for: service)
            }
        }
    }

    // confirm we've discovered characteristics
    // of interest within services of interest
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        for characteristic in service.characteristics! {
            print(characteristic)
            if characteristic.uuid == MoveHub_Characteristic_UUID {
                // subscribe to a single notification
                // for characteristic of interest;
                // "When you call this method to read
                // the value of a characteristic, the peripheral
                // calls ... peripheral:didUpdateValueForCharacteristic:error:
                //
                // Read    Mandatory
                //
                selectedCharacteristic = characteristic
                //peripheral.readValue(for: characteristic)
            }
        }
    }
    
    // we're notified whenever a characteristic
    // value updates regularly or posts once; read and
    // decipher the characteristic value(s) that we've
    // subscribed to
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        if characteristic.uuid == MoveHub_Characteristic_UUID {
            // we generally have to decode BLE
            // data into human readable format
            //let heartRate = deriveBeatsPerMinute(using: characteristic)
            DispatchQueue.main.async { () -> Void in
              /*  UIView.animate(withDuration: 1.0, animations: {
                    self.beatsPerMinuteLabel.alpha = 1.0
                    self.beatsPerMinuteLabel.text = String(heartRate)
                }, completion: { (true) in
                    self.beatsPerMinuteLabel.alpha = 0.0
                })
                */
                //print(characteristic.value)
            }
        }
    }
    
    func send(command: String){
        print("Contoller sending");
        print(command);
        print("...")
        //todo sending command
        //0c0081371109640032647f03
        var bytes = [UInt8]()
        let text = command//"0c0081371109640032647f03"
        //0c0081381109640032647f03
        //0c0081391109e80364647f03 2 à 100%
        //0d008139110ae803649b647f03 turn
        for i in stride(from: text.startIndex.encodedOffset, to: text.endIndex.encodedOffset, by: 2){
            let hexByte = text[i..<i.advanced(by: 2)]
            if let byte:UInt8 = UInt8(hexByte, radix:16) {
                bytes.append(byte)
            }
        }
        let data = NSData(bytes: bytes, length: bytes.count)
        writeDataToSelectedCharacteristic(data: data)
    }
    
    func startOpportunity() {
        if central.state == .poweredOn && started {
            print("Scanning...")
            central.scanForPeripherals(withServices:[MoveHub_Service_UUID], options: [CBCentralManagerScanOptionAllowDuplicatesKey:false])
        }
    }

    func start() {
        started = true
        print("started")
        startOpportunity()
    }

    func writeDataToSelectedCharacteristic(data:NSData) {
        //log("writing data \(data)")
        if let characteristic = selectedCharacteristic {
            var writeType = CBCharacteristicWriteType.withResponse
            if (!characteristic.properties.contains(.write)) {
                writeType = .withoutResponse
            }
            connectedPeripheral?.writeValue(data as Data, for: characteristic, type: writeType)
        }
    }
}

extension String {
    subscript(_ range: CountableRange<Int>) -> String {
        let idx1 = index(startIndex, offsetBy: max(0, range.lowerBound))
        let idx2 = index(startIndex, offsetBy: min(self.count, range.upperBound))
        return String(self[idx1..<idx2])
    }
}
